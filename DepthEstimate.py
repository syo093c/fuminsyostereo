from tqdm import tqdm
import argparse
import copy
import cv2 as cv
import matplotlib.pyplot as plt
import megengine as mge
import megengine.functional as F
import numpy as np
import open3d as o3d
import os
import pickle
import sys
import tensorflow as tf
sys.path.append('./CREStereo')
from CREStereo.nets import Model 
from CREStereo.test import load_model

#np.set_printoptions(threshold=sys.maxsize)
sys.path.append('./hitnet')
from hitnet import HitNet, ModelType, draw_disparity, draw_depth, CameraConfig, load_img

def split_image():
    img=cv.imread('000480.png')
    imgL=img[:img.shape[0],:int(img.shape[1]/2)]
    imgR=img[:img.shape[0],int(img.shape[1]/2):]
    cv.imwrite(filename='imgL.png',img=imgL)
    cv.imwrite(filename='imgR.png',img=imgR)

class DepthEstimate():
    def __init__(self,model='',need_to_calibrate=1):
        with open('./stereo_camera_calibration_result.pkl','rb') as f:
            self.camera_parameter=pickle.load(f)
        self.model=model
        self.need_to_calibrate=need_to_calibrate
        #self.img=self.load_img(img)
        #self.convert2pcd()
    
    def update(self,img):
        self.img=self.load_img(img)
        self._width=self.img_left.shape[1]
        self._height=self.img_left.shape[0]

    def CREStereo(self):
        n_iter=20
        left=self.img_left
        right=self.img_right

        imgL = left.transpose(2, 0, 1)
        imgR = right.transpose(2, 0, 1)
        imgL = np.ascontiguousarray(imgL[None, :, :, :])
        imgR = np.ascontiguousarray(imgR[None, :, :, :])

        imgL = mge.tensor(imgL).astype("float32")
        imgR = mge.tensor(imgR).astype("float32")

        imgL_dw2 = F.nn.interpolate(
            imgL,
            size=(imgL.shape[2] // 2, imgL.shape[3] // 2),
            mode="bilinear",
            align_corners=True,
        )
        imgR_dw2 = F.nn.interpolate(
            imgR,
            size=(imgL.shape[2] // 2, imgL.shape[3] // 2),
            mode="bilinear",
            align_corners=True,
        )
        pred_flow_dw2 = self.model(imgL_dw2, imgR_dw2, iters=n_iter, flow_init=None)

        pred_flow = self.model(imgL, imgR, iters=n_iter, flow_init=pred_flow_dw2)
        pred_disp = F.squeeze(pred_flow[:, 0, :, :]).numpy()
        return pred_disp



    # Traditional method borrow from undergraduate students
    # https://github.com/pubgeo/dfc2019/blob/master/track2/test-sgbm.py
    def sgbm(self):
        # run SGM stereo matching with weighted least squares filtering
        #print('Running SGBM stereo matcher...')
        if len(self.img_left.shape) > 2:
            rimg1 = cv.cvtColor(self.img_left, cv.COLOR_BGR2GRAY)
            rimg2 = cv.cvtColor(self.img_right, cv.COLOR_BGR2GRAY)
        window_size = 5
        # Max disparity determind how close you can measure.
        left_matcher = cv.StereoSGBM_create(
            #minDisparity=12,   # 10m
            #numDisparities=400,# 0.3m
            minDisparity=6,     # 20m
            numDisparities=128, # 1m
            blockSize=window_size,
            #blockSize=13,
            P1=8 * 3 * window_size ** 2,
            P2=32 * 3 * window_size ** 2,
            disp12MaxDiff=1,
            uniquenessRatio=15,
            #speckleWindowSize=500,
            speckleWindowSize=50,
            speckleRange=2,
            preFilterCap=63,
            mode=cv.STEREO_SGBM_MODE_SGBM_3WAY
        )
        right_matcher = cv.ximgproc.createRightMatcher(left_matcher)
        lmbda = 8000
        sigma = 1.5
        wls_filter = cv.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
        wls_filter.setLambda(lmbda)
        wls_filter.setSigmaColor(sigma)
        displ = left_matcher.compute(rimg1, rimg2)
        dispr = right_matcher.compute(rimg2, rimg1)
        displ = np.int16(displ)
        dispr = np.int16(dispr)
        disparity = wls_filter.filter(displ, rimg1, None, dispr) / 16.0
        disparity[np.where(disparity<0)] = 0
        disparity=disparity.astype('float32')
        self.disparity_map=disparity
        return disparity


    def _estimate_depth(self):
        #self.disparity_map=self.model(self.img_left,self.img_right)
        self.disparity_map=self.CREStereo()
        #self.disparity_map=self.sgbm()
        self.disparity_map= np.where(self.disparity_map>12,self.disparity_map,12)

    def load_img(self,img):
        #img=cv.imread(path)
        #img=cv.resize(img,(img.shape[1],img.shape[0]),interpolation= cv.INTER_LINEAR)
        #img=cv.resize(img,(img.shape[1]//2,img.shape[0]//2),interpolation= cv.INTER_LINEAR)
        imgL=img[:img.shape[0],:int(img.shape[1]/2)]
        imgR=img[:img.shape[0],int(img.shape[1]/2):]
        
        m1=self.camera_parameter['M1']
        dist1=self.camera_parameter['dist1']
        m2=self.camera_parameter['M2']
        dist2=self.camera_parameter['dist2']
        h,  w = imgL.shape[:2]
        size=(h,w)
        r=self.camera_parameter['R']
        t=self.camera_parameter['T']

        R1, R2, P1, P2, Q, roi_l, roi_r= cv.stereoRectify(m1,dist1,m2,dist2,size,r,t)
        mapx, mapy = cv.initUndistortRectifyMap(m1, dist1,R1, P1, (w,h), 5)
        dst_l = cv.remap(imgL, mapx, mapy, cv.INTER_LINEAR)
        mapx, mapy = cv.initUndistortRectifyMap(m2, dist2, R2, P2, (w,h), 5)
        dst_r = cv.remap(imgR, mapx, mapy, cv.INTER_LINEAR)

        if self.need_to_calibrate==0:
            #print('the image has alread been calibrated')
            #We have all readly generate calibrated video, so that
            #do not need to remap image again.
            #And Openpose will generate keypoint for calibrated video
            self.img_left=imgL
            self.img_right=imgR
        else:
            #print('calibrate input image')
            self.img_left=dst_l
            self.img_right=dst_r
        self.Q=Q

        #self.c_x = self.img_left.shape[1]//2
        #self.c_y = self.img_left.shape[0]//2
        #self.T_x = 0.12  # baseline
        self.c_x = -self.Q[0][3]
        self.c_y = -self.Q[1][3]
        self.f = self.Q[2][3]
        self.c_xt = self.c_x
        self.T_x = 1/self.Q[3][2]  # baseline
        #print(self.Q)
        #print(self.c_x)
        #print(self.c_y)
        #print(self.f)
        #print(self.T_x)
        #self.Q = np.array([
        #    [1, 0, 0, -self.c_x],
        #    [0, 1, 0, -self.c_y],
        #    [0, 0, 0, self.f],
        #    [0, 0, 1/0.12, 0]
        #])
        #print(self.camera_parameter)
        self._estimate_depth()
        self._estimate_3d()
        
    def _estimate_3d(self):
        #print(self.Q)
        self.img_3d=cv.reprojectImageTo3D(disparity=self.disparity_map,Q=self.Q) 
        
        #points(xyz) for open3d format
        self.xyz = np.zeros((self.img_3d.shape[0]*self.img_3d.shape[1], 3))
        x=np.reshape(self.img_3d[:,:,0],-1)
        y=np.reshape(self.img_3d[:,:,1],-1)
        z=np.reshape(self.img_3d[:,:,2],-1)
        self.xyz[:,0]=x
        self.xyz[:,1]=y
        self.xyz[:,2]=z

    def to_camera_coordinate(self,pixel_point):
        # deepth, (x,y) to (X,Y,Z)
        #x*deepth/self.f
        return self.img_3d[pixel_point[0],pixel_point[1]]


    def plot_disparity_map(self):
        self.color_disparity = draw_disparity(self.disparity_map)
        cobined_image = np.hstack((self.img_left, self.img_right, self.color_disparity))
        cv.namedWindow("Estimated disparity", cv.WINDOW_NORMAL)	
        cv.imshow("Estimated disparity", self.color_disparity)
        cv.waitKey(0)
        cv.imwrite("out.jpg", cobined_image)
        cv.imwrite("disparity.jpg", self.color_disparity)
        cv.destroyAllWindows()

    def convert2pcd(self):
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(self.xyz)
        self.pcd=pcd
        o3d.io.write_point_cloud("./test.ply", pcd)
        
        #for OpenPCDet
        x=self.xyz[:,0]
        y=self.xyz[:,1]
        z=self.xyz[:,2]
        n = np.zeros((self.img_3d.shape[0]*self.img_3d.shape[1], 4))
        n[:,0]=z
        n[:,1]=-x
        n[:,2]=-(y+1.)
        n[:,3]=0
        np.save('test.npy',n)
        #pcd_load = o3d.io.read_point_cloud("./test.ply")
        #coord=o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.5)
        #o3d.visualization.draw_geometries([pcd_load,coord])

    def visualization_rgb(self):
        rgbd_image=o3d.geometry.RGBDImage.create_from_color_and_depth(o3d.geometry.Image(cv.cvtColor(self.img_left, cv.COLOR_BGR2RGB)),
                    o3d.geometry.Image(1054*0.12/self.disparity_map),convert_rgb_to_intensity=False)
        pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
                rgbd_image,
                o3d.camera.PinholeCameraIntrinsic(1920,1080,1054,1054,1920/2,1080/2))
        coord=o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.0005)
        o3d.visualization.draw_geometries([pcd,coord])



def main():
    #img=cv.imread('/home/syo/work/dataset/classroom_pr/scence1/000018.png')
    img=cv.imread('mpv-shot0002.jpg')
    #img=cv.imread('dataset/output.png')
    #img=cv.imread('/home/syo/work/classroom/test2.png')
    #img=cv.imread('/home/syo/work/classroom/Explorer_HD1080_SN27459229_20-34-37.png')
    img=cv.resize(img,(img.shape[1],img.shape[0]),interpolation= cv.INTER_LINEAR)
    #img=cv.resize(img,(img.shape[1]//2,img.shape[0]//2),interpolation= cv.INTER_LINEAR)
    #img=cv.resize(img,(img.shape[1]//2,img.shape[0]//2),interpolation= cv.INTER_LINEAR)
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    dpe.update(img)
    dpe.plot_disparity_map()
    dmap=np.where(dpe.disparity_map>12,dpe.disparity_map,0)
    print(dmap.shape)
    rgbd_image=o3d.geometry.RGBDImage.create_from_color_and_depth(o3d.geometry.Image(cv.cvtColor(dpe.img_left, cv.COLOR_BGR2RGB)),
                o3d.geometry.Image(1054*0.12/dmap),convert_rgb_to_intensity=False)
    pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
            rgbd_image,
            o3d.camera.PinholeCameraIntrinsic(1920,1080,1054,1054,1920/2,1080/2))

    coord=o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.0005)
    o3d.visualization.draw_geometries([pcd,coord])


    
def test_cloud_point_fusion_with_rgb():
    img_font=cv.imread('result.png')
    #img_back=cv.imread('c1-2.png')
    dpe_font=DepthEstimate(img_font)
    rgbd_image=o3d.geometry.RGBDImage.create_from_color_and_depth(o3d.geometry.Image(cv.cvtColor(dpe_font.img_left,cv.COLOR_BGR2GRAY)),
                o3d.geometry.Image(1054*0.12/dpe_font.disparity_map))
    pcd_font = o3d.geometry.PointCloud.create_from_rgbd_image(
            rgbd_image,
            o3d.camera.PinholeCameraIntrinsic(1920,1080,1054,1054,1920/2,1080/2))
    
    img_back=cv.imread('000480.png')
    #img_back=cv.imread('c1-2.png')
    dpe_back=DepthEstimate(img_back)
    rgbd_image=o3d.geometry.RGBDImage.create_from_color_and_depth(o3d.geometry.Image(cv.cvtColor(dpe_back.img_left, cv.COLOR_BGR2GRAY)),
                o3d.geometry.Image(1054*0.12/dpe_back.disparity_map))
    pcd_back = o3d.geometry.PointCloud.create_from_rgbd_image(
            rgbd_image,
            o3d.camera.PinholeCameraIntrinsic(1920,1080,1054,1054,1920/2,1080/2),
            np.array(
                [[-1,0,0,0],
                 [0,1,0,0],
                 [0,0,-1,0.005],
                 [0,0,0,1]]
            )
            )

    coord=o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.0005)
    #o3d.visualization.draw_geometries([dpe_font.pcd,dpe_back.pcd])
    o3d.visualization.draw_geometries([pcd_font,pcd_back,coord])


def demo2(video_path):
    #os.mkdir('video_tmp')
    cap = cv.VideoCapture(video_path)
    n=0
    frame_number=int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    for i in tqdm(range(frame_number)):
        n+=1
        ret,frame=cap.read()
        if not ret:
            break
        
        dpe=DepthEstimate(frame)
        disparity_img=draw_disparity(dpe.disparity_map)
        #cv.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',disparity_img)
        a=[(1002, 755), (1009, 731), (1042, 718.113), (1026.09, 714.598), (1036.33, 711.405), (1019.4, 711.318), (1022.73, 707.925), (1009.09, 704.537)]
        y=int(a[n-1][1])
        x=int(a[n-1][0])
        print(1000*0.12/dpe.disparity_map[y-50:y+50,x-50:x+50].mean())
        print(1000*0.12/dpe.disparity_map[y,x])
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+'disparity.avi'
    os.system(cmd)

def video_to_disparity(video_path):
    os.mkdir('video_tmp')
    cap = cv.VideoCapture(video_path)
    n=0
    frame_number=int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    for i in tqdm(range(frame_number)):
        n+=1
        ret,frame=cap.read()
        if not ret:
            break
        
        dpe=DepthEstimate(frame)
        disparity_img=draw_disparity(dpe.disparity_map)
        cv.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',disparity_img)
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+'disparity_video.avi'
    os.system(cmd)

def calibration_test():
    #img=cv.imread('./calibration_test/mpv-shot0001.jpg')
    img=cv.imread('./calibration_test/a.png')

    #img=cv.resize(img,(img.shape[1],img.shape[0]),interpolation= cv.INTER_LINEAR)
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    dpe.update(img)
    #dpe.plot_disparity_map()

    #save img
    cv.imwrite('l.png', dpe.img_left)
    cv.imwrite('r.png', dpe.img_right)

    #opencv 4.7 has dead. use colab instead.    
    chess_board_size=(14,9)
    flags = cv.CALIB_CB_NORMALIZE_IMAGE | cv.CALIB_CB_EXHAUSTIVE | cv.CALIB_CB_ACCURACY
    #gray_l = cv.cvtColor(dpe.img_left, cv.COLOR_BGR2GRAY)
    #gray_r = cv.cvtColor(dpe.img_right, cv.COLOR_BGR2GRAY)
    #print(gray_l)
    ##print(i)
    ## Find the chess board corners
    #ret_l, corners_l = cv.findChessboardCornersSB(gray_l, (chess_board_size[0], chess_board_size[1]), flags)
    #ret_r, corners_r = cv.findChessboardCornersSB(gray_r, (chess_board_size[0],chess_board_size[1]), flags)

    #print(ret_l)
    #print(ret_r)

if __name__ == '__main__':
    #split_image()
    #main()
    #demo2('test/test.avi')
    #video_to_disparity('test.avi')
    #test_mesh()
    #test_cloud_point_fusion()
    #test_cloud_point_fusion_with_rgb()
    calibration_test()
