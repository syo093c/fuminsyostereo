from DepthEstimate import DepthEstimate

import cv2 as cv
from CREStereo.test import load_model
import numpy as np
import open3d as o3d
import sys
import argparse
from tqdm import tqdm

from pathlib import Path
from Detection2d import Predictor
#np.set_printoptions(threshold=sys.maxsize)

import torch
from loguru import logger
from yolox.exp import get_exp
from yolox.utils import fuse_model, get_model_info, postprocess, vis
from mmpose.apis import (inference_top_down_pose_model, init_pose_model,
                         vis_pose_result)
from yolox.data.datasets import COCO_CLASSES
from utils import open3d_vis_utils as V
from utils.Visualiz import Visualiz
import open3d
import os 

def init_detection2d(exp=None,ckpt=None,use_pose_model=False):
    """
    init 2d detector use yolox
    @exp.test_conf is the thred value for confidence
    """
    if exp is None:
        exp = get_exp(exp_name='yolox-x')
        #exp.test_conf=0.7
    model = exp.get_model()
    logger.info("Model Summary: {}".format(get_model_info(model, exp.test_size)))
    model.cuda()
    model.eval()
    logger.info("loading checkpoint")
    if ckpt is None:
        ckpt = torch.load('detection/YOLOX/weight/yolox_x.pth', map_location="cpu")
    else:
        ckpt=torch.load(ckpt,map_location='cpu')
    # load the model state dict
    model.load_state_dict(ckpt["model"])
    logger.info("loaded checkpoint done.")

    predictor = Predictor(
        model, exp, COCO_CLASSES,use_pose_model=use_pose_model
    )
    print(exp)
    return predictor


class Detection3d():
    def __init__(self,detection2d:Predictor,dpe:DepthEstimate) -> None:
        self.det2d=detection2d
        self.dpe=dpe
        self.visualizer=Visualiz()

    @staticmethod
    def generate_person_template(center,lwh,rotation=0):
        return np.array([*center,*lwh,rotation])
        
    def _warp_bbox2d_result(self,bbox):
        bbox=bbox.copy()
        # warp bbox2d result to fit image pixel size
        x0 = bbox[0]
        y0 = bbox[1]
        x1 = bbox[2]
        y1 = bbox[3]
        x0 = max(0.,x0)
        x0 = min(self.dpe._width-1,x0)
        x1 = max(0.,x1)
        x1 = min(self.dpe._width-1,x1)
        y0 = max(0.,y0)
        y0 = min(self.dpe._height-1,y0)
        y1 = max(0.,y1)
        y1 = min(self.dpe._height-1,y1)
        bbox[0] = x0
        bbox[1] = y0
        bbox[2] = x1
        bbox[3] = y1
        return  bbox


    @staticmethod
    def occlused(a,b)-> bool:
        #tlbr
        iw=( min(a[2], b[2]) - max(a[0], b[0]) + 1)
        if iw > 0:
            ih = ( min(a[3], b[3]) - max(a[1], b[1]) + 1)
            if ih >0:
                return True
        return False
        
    def occlusion_filter(self):
        """
            Filtering wrong 3d detection results.
            For occlusion situation, 2d detection will give some bbox with occlusion,
            deepth estimation for occlued bbox may failed.

            Remove failed detection.
            1. The largest box may be the closet box.
            2. Remove the box whose center is in largest box.
            
            example:
            0      1    2
            ----------
            |     ________
            |     |  |  _|
            |   * | *| |*|
            |     |  | |_|
            |     -------- 
            ----------
            1 and 2 should be removed, although 2 is not occlued with 0, but it
            occlued with 1.

        """
        new_bboxes=[]
        new_persons_center=[]
        remove_list=[]
        for i in range(len(self.bboxes)):
            for j in range(i,len(self.bboxes)):
                if i == j : continue
                if self.occlused(self.bboxes[i],self.bboxes[j]):
                    # compare size to determind that who is closer
                    size_i=(self.bboxes[i][2]-self.bboxes[i][0])*(self.bboxes[i][3]-self.bboxes[i][1])
                    size_j=(self.bboxes[i][2]-self.bboxes[i][0])*(self.bboxes[i][3]-self.bboxes[i][1])
                    # if smaller bbox's center in larger bbox
                    # remove the smaller one 
                    if size_i < size_j:
                        _=i
                        i=j
                        j=_
                        # now size_i > size_j 

                    #human center in box
                    x=self.persons_center[j][0]
                    y=self.persons_center[j][1]
                    if self.bboxes[i][0]<x and x< self.bboxes[i][2] and self.bboxes[i][1]<y and y< self.bboxes[i][3]:
                        remove_list.append(j)

                    # box center
                    #x=0.5*(self.bboxes[j][0]+self.bboxes[j][2])
                    #y=0.5*(self.bboxes[j][1]+self.bboxes[j][3])
                    #if self.bboxes[i][0]<x and x< self.bboxes[i][2] and self.bboxes[i][1]<y and y< self.bboxes[i][3]:
                    #    remove_list.append(j)

        for i in range(len(self.bboxes)):
            if i not in remove_list:
                new_bboxes.append(self.bboxes[i])
                new_persons_center.append(self.persons_center[i])

        #print(self.bboxes)
        self.bboxes=np.array(new_bboxes)
        self.persons_center=np.array(new_persons_center)
        #print(self.bboxes)

    def update_with_pose(self,img):
        """
        use detect_human function to update @self.bboxes resized 2d bboxes
        """
        self.dpe.update(img)
        self.img_left=self.dpe.img_left

        #get person 2d bbox
        #output,classes = self.det2d.detect(self.img_left)
        output = np.array(self.det2d.detect_human_with_pose(self.img_left)) #top left, bottom right
        self.disparity_map=np.array(self.dpe.disparity_map)

        #add skeleton information
        bboxes=[i['bbox'] for i in output]
        keypoints=[i['keypoints'] for i in output]
        self.bboxes=bboxes
        self.skeletons=keypoints

        # update 3d bboxes
        #TODO: occlusion situation, determind depth
        bboxes_3d=[]
        _bboxes=[]
        for bbox in self.bboxes:
            bbox=self._warp_bbox2d_result(bbox)
            _bboxes.append(bbox)
        self.bboxes=np.array(_bboxes)

        # 2d occlusion filter
        self.occlusion_filter()

        for bbox in self.bboxes:
            x0 = bbox[0]
            y0 = bbox[1]
            x1 = bbox[2]
            y1 = bbox[3]

            center = np.array([(y0+y1)/2., (x0+x1)/2.]).astype(int)
            tl = self.dpe.to_camera_coordinate(center)
            _ex = abs(x0-x1)
            _ey = abs(y0-y1)
            ex = _ex*tl[2]/self.dpe.f
            ey = _ey*tl[2]/self.dpe.f

            box = self.generate_person_template(tl, [ex, ey, ex]) # center, ex, ey, ez, rotation:0
            bboxes_3d.append(box)
        self.bboxes_3d=bboxes_3d 
        self.scores=np.array(self.bboxes[:,4])

        #update 3d skeleton
        #TODO: occlusion situation, determind depth
        #print(self.skeletons)
        skeletons_3d=[]
        for skeleton in self.skeletons:
            skeleton_3d=[]
            for point in skeleton:
                # skeleton points are possiably out of image.
                # just move them to image.
                y,x,_=self.dpe.img_left.shape # y:1080 x:1920
                if point[0] >= x:
                    point[0] = x-1
                if point[1] >= y:
                    point[1] = y-1
                point3d=self.dpe.to_camera_coordinate(np.array([point[1],point[0]]).astype(int)) # dpe.image_3d: (1080,1920,3)
                skeleton_3d.append(point3d)
            skeletons_3d.append(np.array(skeleton_3d))

        self.skeletons_3d=skeletons_3d
        
    def update(self,img):
        """
        use detect_human function to update @self.bboxes resized 2d bboxes
        """
        self.dpe.update(img)
        self.img_left=self.dpe.img_left

        #get person 2d bbox
        #output,classes = self.det2d.detect(self.img_left)
        output = np.array(self.det2d.detect_human(self.img_left)) #top left, bottom right
        self.disparity_map=np.array(self.dpe.disparity_map)

        #add skeleton information
        # with score
        self.bboxes=output[:,:5]

        # update 3d bboxes
        #TODO: occlusion situation, determind depth
        bboxes_3d=[]
        _bboxes=[]
        for bbox in self.bboxes:
            bbox=self._warp_bbox2d_result(bbox)
            _bboxes.append(bbox)
        self.bboxes=np.array(_bboxes)


        # get human pose (only closet person)
        person_results=[{'bbox':i.tolist()} for i in self.bboxes]
        #print(person_results)
        pose_results, returned_outputs=inference_top_down_pose_model(
        self.det2d.pose_model,
        self.img_left,
        person_results,
        bbox_thr=None,
        format='xyxy',
        dataset=self.det2d.pose_dataset,
        dataset_info=self.det2d.pose_dataset_info,
        return_heatmap=False,
        outputs=None)

        #bboxes=[i['bbox'] for i in pose_results]
        self.keypoints=[i['keypoints'] for i in pose_results]
        #self.persons_center=[np.array((self.keypoints[i][10]+self.keypoints[i][11])/2.0)[:2].astype(int) for i in range(len(self.keypoints))]
        self.persons_center=[np.array(self.keypoints[i][10])[:2].astype(int) for i in range(len(self.keypoints))]

        # 2d occlusion filter
        self.occlusion_filter()

        for i,bbox in enumerate(self.bboxes):
            # just use center point to determind deepth
            x0 = bbox[0]
            y0 = bbox[1]
            x1 = bbox[2]
            y1 = bbox[3]

            center1 = np.array([(y0+y1)/2., (x0+x1)/2.]).astype(int)
            #print(center1)
            
            # use hip point to determind deepth
            center2=self.persons_center[i][::-1]
            #print(center2)
            #print(self.dpe.disparity_map[center1[0],center1[1]])

            # use histogram to detemind deepth

            tl1 = self.dpe.to_camera_coordinate(center1)
            tl2 = self.dpe.to_camera_coordinate(center2)

            person_deepth=tl2[2]
            tl=np.array([(center1[1]-self.dpe.c_x)*person_deepth/self.dpe.f,(center1[0]-self.dpe.c_y)*person_deepth/self.dpe.f,person_deepth])
            #print(tl1)
            #print(tl)
            #print('-------------')
            #tl=np.array([tl1[0],tl1[1],tl2[2]])
            #tl=tl2
            _ex = abs(x0-x1)
            _ey = abs(y0-y1)
            ex = _ex*tl[2]/self.dpe.f
            ey = _ey*tl[2]/self.dpe.f

            box = self.generate_person_template(tl, [ex, ey, ex]) # center, ex, ey, ez, rotation:0
            bboxes_3d.append(box)
        self.bboxes_3d=bboxes_3d 
        self.scores=np.array(self.bboxes[:,4])


    def visualization(self):
        # Open3D Image use (0-1) as color range
        image = cv.cvtColor(self.dpe.img_left,cv.COLOR_BGR2RGB)/255.
        point_colors=image.reshape(-1,3)

        boxes=self.bboxes_3d
        #skeletons=self.skeletons_3d
        skeletons=None

        if not self.visualizer.canvas_inited:
            self.visualizer.init_canvas()
        img=self.visualizer.update(
            points=self.dpe.xyz,
            #points=None,
            ref_boxes=np.array(boxes),
            point_colors=point_colors,
            skeletons=skeletons,
        )
        return img

    def visualization_2d(self):
        img=self.det2d.visual(img=self.dpe.img_left,result=self.bboxes)
        cv.imshow(mat=img,winname='test')
        cv.waitKey(0)


def img(opt):
    #img=cv.imread('mpv-shot0003.jpg')
    img=cv.imread(opt.input)
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    config='mmsegmentation/configs/pspnet/pspnet_r101-d8_512x512_4x4_80k_coco-stuff164k.py'
    checkpoint='mmsegmentation/weight/pspnet_r101-d8_512x512_4x4_80k_coco-stuff164k_20210707_152034-7eb41789.pth'
    device='cuda:0'
    det2d_model = init_detection2d()

    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)

    det3d=Detection3d(det2d_model,dpe)
    det3d.update(img)
    det3d.visualization()

def video(opt):
    if os.path.exists('video_tmp'):
        os.system("rm -rf ./video_tmp")
    os.mkdir("video_tmp")

    #img=cv.imread('mpv-shot0003.jpg')
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    config='mmsegmentation/configs/pspnet/pspnet_r101-d8_512x512_4x4_80k_coco-stuff164k.py'
    checkpoint='mmsegmentation/weight/pspnet_r101-d8_512x512_4x4_80k_coco-stuff164k_20210707_152034-7eb41789.pth'
    device='cuda:0'
    det2d_model = init_detection2d(use_pose_model=True)
    #det2d_model = init_detection2d()
    #dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)
    det3d=Detection3d(det2d_model,dpe)

    cap=cv.VideoCapture(opt.input)
    n=0
    frame_number=int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    print(frame_number)

    #save_path='video.mp4'
    #fps=30
    #width=1920
    #height=1080
    #vid_writer = cv.VideoWriter(
    #save_path, cv.VideoWriter_fourcc(*"mp4v"), fps, (int(width), int(height))
    #)

    for n in tqdm(range(frame_number)):
        ret,frame=cap.read()
        n+=1
        if not ret:
            break
        img=frame
        det3d.update(img)
        img=det3d.visualization()
        cv.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',img)
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+'3d_detection_result_'+os.path.basename(opt.input)
    os.system(cmd) 

if __name__ == '__main__':
    arg=argparse.ArgumentParser()
    arg.add_argument('function',default='video')
    arg.add_argument('--input','-i',default='test1.mp4')
    opt=arg.parse_args()
    if opt.function == 'img':
        img(opt)
    if opt.function == 'video':
        video(opt)
