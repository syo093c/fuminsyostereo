import cv2 as cv
import tensorflow as tf
import numpy as np
import copy
import open3d as o3d
import sys
import matplotlib.pyplot as plt
import pickle
from tqdm import tqdm
import os

#np.set_printoptions(threshold=sys.maxsize)
sys.path.append('./hitnet')
from hitnet import HitNet, ModelType, draw_depth, CameraConfig, load_img

sys.path.append('./CREStereo')
from CREStereo.test import load_model,draw_disparity
from DepthEstimate import DepthEstimate
from StereoCalibration import generate_calibrated_video
import argparse

def main(video_path,output):
    if os.path.exists('video_tmp'):
        os.system('rm -rf ./video_tmp')
    os.mkdir('video_tmp')

    cap = cv.VideoCapture(video_path)
    n=0
    frame_number=int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    
    #CREStereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    #Hitenet_model=HitNet("hitnet/models/eth3d.pb",ModelType.eth3d)

    for i in tqdm(range(frame_number)):
        n+=1
        ret,frame=cap.read()
        if not ret:
            break
        
        #dpe=DepthEstimate(frame,model=CREStereo_model)
        #dpe=DepthEstimate(frame,model=Hitenet_model)
        dpe=DepthEstimate(frame)
        disparity_img=draw_disparity(dpe.disparity_map)
        cv.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',disparity_img)
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+ output
    os.system(cmd)

if __name__ == '__main__':
    arg=argparse.ArgumentParser()
    arg.add_argument('--input_video','-i',default='')
    arg.add_argument('--output_video','-o',default='')
    opt=arg.parse_args()
    main(opt.input_video,opt.output_video)