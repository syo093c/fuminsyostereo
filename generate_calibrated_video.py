import cv2 as cv
import tensorflow as tf
import numpy as np
import copy
import open3d as o3d
import sys
import matplotlib.pyplot as plt
import pickle
from tqdm import tqdm
import os
import argparse

#np.set_printoptions(threshold=sys.maxsize)
sys.path.append('./hitnet')
from hitnet import HitNet, ModelType, draw_disparity, draw_depth, CameraConfig, load_img

from DepthEstimate import DepthEstimate

from StereoCalibration import generate_calibrated_video


def main(video,output):
    generate_calibrated_video(video,output)

if __name__=='__main__':
    paser=argparse.ArgumentParser()
    paser.add_argument('--video','-v',type=str)
    paser.add_argument('--output','-o',type=str)
    args=paser.parse_args()

    main(args.video,args.output)