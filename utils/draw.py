import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import argparse
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

def moving_average(x,y,z,window_size=10):
    x_ma = np.convolve(x, np.ones(window_size)/window_size, mode='valid')
    y_ma = np.convolve(y, np.ones(window_size)/window_size, mode='valid')
    z_ma = np.convolve(z, np.ones(window_size)/window_size, mode='valid')
    return x_ma,y_ma,z_ma

def va(opt):
    # read data from csv, dataframe
    data=pd.read_csv(opt.input)
    
    ax = plt.figure().add_subplot(projection='3d')

    colors = [(144, 238, 144), (178, 34, 34), (221, 160, 221), (0, 255, 0), (0, 128, 0), (210, 105, 30), (220, 20, 60),
                 (192, 192, 192), (255, 228, 196), (50, 205, 50), (139, 0, 139), (100, 149, 237), (138, 43, 226),
                 (238, 130, 238),
                 (255, 0, 255), (0, 100, 0), (127, 255, 0), (255, 0, 255), (0, 0, 205), (255, 140, 0), (255, 239, 213),
                 (199, 21, 133), (124, 252, 0), (147, 112, 219), (106, 90, 205), (176, 196, 222), (65, 105, 225),
                 (173, 255, 47),
                 (255, 20, 147), (219, 112, 147), (186, 85, 211), (199, 21, 133), (148, 0, 211), (255, 99, 71),
                 (144, 238, 144),
                 (255, 255, 0), (230, 230, 250), (0, 0, 255), (128, 128, 0), (189, 183, 107), (255, 255, 224),
                 (128, 128, 128),
                 (105, 105, 105), (64, 224, 208), (205, 133, 63), (0, 128, 128), (72, 209, 204), (139, 69, 19),
                 (255, 245, 238),
                 (250, 240, 230), (152, 251, 152), (0, 255, 255), (135, 206, 235), (0, 191, 255), (176, 224, 230),
                 (0, 250, 154),
                 (245, 255, 250), (240, 230, 140), (245, 222, 179), (0, 139, 139), (143, 188, 143), (255, 0, 0),
                 (240, 128, 128),
                 (102, 205, 170), (60, 179, 113), (46, 139, 87), (165, 42, 42), (178, 34, 34), (175, 238, 238),
                 (255, 248, 220),
                 (218, 165, 32), (255, 250, 240), (253, 245, 230), (244, 164, 96), (210, 105, 30)]
    colors=np.array(colors)/255.0

    id_list= data['track_id'].unique()
    for i in id_list:
        d=data[data['track_id']==i]
        c=colors[int(i)%len(colors)]
        x = d['x']
        y = d['y']
        z = d['z']
        x,y,z=moving_average(x,y,z,window_size=40) #filter

        #ax.scatter(x, y, zs=z,c=c)
        #ax.plot(x, y, zs=z,c=c)
        analyze(x,y,z)


def analyze(x,y,z):
    n=len(x)
    t = np.linspace(0, n, num=n)
    print(t)
# 插值函数
    f_x = interp1d(t, x)
    f_y = interp1d(t, y)
    f_z = interp1d(t, z)

# 新时间序列
    #t_new = np.linspace(0, n, num=n//30)
    t_new = np.linspace(0, n, num=n)
    print(t_new)
    x_new = f_x(t_new)
    y_new = f_y(t_new)
    z_new = f_z(t_new)

# 计算速度
    v_x = np.gradient(x_new, t_new)
    v_y = np.gradient(y_new, t_new)
    v_z = np.gradient(z_new, t_new)
    v_x*=30
    v_y*=30
    v_z*=30

# 计算加速度
    a_x = np.gradient(v_x, t_new)
    a_y = np.gradient(v_y, t_new)
    a_z = np.gradient(v_z, t_new)
    a_x*=30
    a_y*=30
    a_z*=30

# 计算运动方向
    v_z[abs(v_z)<0.15]=0
    v_x[abs(v_x)<0.15]=0
    d = np.arctan2(v_z, v_x)

    window_size=10
    d=np.convolve(d, np.ones(window_size)/window_size, mode='valid')

# 绘制速度和加速度曲线
    fig, axs = plt.subplots(2, 1, figsize=(8, 6), sharex=True)

    #t_new =[i for i in range(len(t_new))] 
    axs[0].plot(t_new, v_x, label='vx')
    axs[0].plot(t_new, v_y, label='vy')
    axs[0].plot(t_new, v_z, label='vz')
    axs[0].set_ylabel('v')
    axs[0].legend()

    axs[1].plot(t_new, a_x, label='ax')
    axs[1].plot(t_new, a_y, label='ay')
    axs[1].plot(t_new, a_z, label='az')
    axs[1].set_xlabel('t')
    axs[1].set_ylabel('a')
    axs[1].legend()

    plt.show()

# 绘制运动方向曲线
    fig, ax = plt.subplots(figsize=(8, 6))

    
    t_new =[i for i in range(len(d))] 
    ax.plot(t_new, d)
    ax.set_xlabel('t')
    ax.set_ylabel('direction')

    plt.show()



def main(opt):
    # read data from csv, dataframe
    data=pd.read_csv(opt.input)
    
    ax = plt.figure().add_subplot(projection='3d')
    #x = data['x']
    #y = data['y']
    #z = data['z']

    colors = [(144, 238, 144), (178, 34, 34), (221, 160, 221), (0, 255, 0), (0, 128, 0), (210, 105, 30), (220, 20, 60),
                 (192, 192, 192), (255, 228, 196), (50, 205, 50), (139, 0, 139), (100, 149, 237), (138, 43, 226),
                 (238, 130, 238),
                 (255, 0, 255), (0, 100, 0), (127, 255, 0), (255, 0, 255), (0, 0, 205), (255, 140, 0), (255, 239, 213),
                 (199, 21, 133), (124, 252, 0), (147, 112, 219), (106, 90, 205), (176, 196, 222), (65, 105, 225),
                 (173, 255, 47),
                 (255, 20, 147), (219, 112, 147), (186, 85, 211), (199, 21, 133), (148, 0, 211), (255, 99, 71),
                 (144, 238, 144),
                 (255, 255, 0), (230, 230, 250), (0, 0, 255), (128, 128, 0), (189, 183, 107), (255, 255, 224),
                 (128, 128, 128),
                 (105, 105, 105), (64, 224, 208), (205, 133, 63), (0, 128, 128), (72, 209, 204), (139, 69, 19),
                 (255, 245, 238),
                 (250, 240, 230), (152, 251, 152), (0, 255, 255), (135, 206, 235), (0, 191, 255), (176, 224, 230),
                 (0, 250, 154),
                 (245, 255, 250), (240, 230, 140), (245, 222, 179), (0, 139, 139), (143, 188, 143), (255, 0, 0),
                 (240, 128, 128),
                 (102, 205, 170), (60, 179, 113), (46, 139, 87), (165, 42, 42), (178, 34, 34), (175, 238, 238),
                 (255, 248, 220),
                 (218, 165, 32), (255, 250, 240), (253, 245, 230), (244, 164, 96), (210, 105, 30)]
    colors=np.array(colors)/255.0

    id_list= data['track_id'].unique()
    for i in id_list:
        d=data[data['track_id']==i]
        c=colors[int(i)%len(colors)]
        x = d['x']
        y = d['y']
        z = d['z']
        x,y,z=moving_average(x,y,z,window_size=10) #filter

        #ax.scatter(x, y, zs=z,c=c)
        ax.plot(x, y, zs=z,c=c)


    #true
    display_gt = False
    #display_gt = True
    if display_gt:
        ground_point=[[-1.29,0,1.46],[1.85,0,1.46],[1.85,8.80,1.46],[-1.29,8.80,1.46],[-1.29,0,1.46]]
        x=[i[0] for i in ground_point]
        z=[i[1] for i in ground_point]
        #y=[i[2] for i in ground_point]
        y=[0.6 for i in ground_point]
        ax.plot(x,y,zs=z,linewidth=2,color='C0')
        ground_point=[[-1.29,3.34,1.46],[1.85,3.37,1.46]]
        x=[i[0] for i in ground_point]
        z=[i[1] for i in ground_point]
        #y=[i[2] for i in ground_point]
        y=[0.6 for i in ground_point]
        ax.plot(x,y,zs=z,linewidth=2,color='C0')

        #keypoint: O, A, B, C, D, E, F, O
        keypoint = [
            [0, 0, 0],
            [-1.29, 8.28, 1.46],
            [-1.29, 6.28, 1.46],
            [-1.29, 4.57, 1.46],
            [1.85, 8.07, 1.46],
            [1.85, 6.30, 1.46],
            [1.85, 4.62, 1.46],
        ]
        x=[i[0] for i in keypoint]
        z=[i[1] for i in keypoint]
        #y=[i[2] for i in keypoint]
        y=[0.6 for i in keypoint]
        keypoint_label = [
            "O",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
        ]
        for i in range(len(x)):
            ax.scatter(x[i], y[i], zs=z[i],s=30)
            offset = 0.05
            ax.text(x[i]+offset, y[i]+offset, z[i]+offset,s=keypoint_label[i], fontsize=12)

    #######


    ax.legend()
    ax.set_ylim(-0.5, 1.5)
    ax.set_xlim(-1.5, 2)
    ax.set_zlim(-0.1, 10)

    #ax.set_zlim(-2, 10)
    #ax.set_ylim(-2, 10)
    #ax.set_xlim(-2, 10)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_box_aspect([1, 1, 2])

    plt.show()
def _main():
    args = argparse.ArgumentParser()
    args.add_argument('--input','-i',default='test1.mp4')
    opt=args.parse_args()
    main(opt)
    va(opt)

def draw_ground():
    ax = plt.figure(dpi=200).add_subplot(projection='3d')
    #ax = plt.figure().add_subplot(projection='3d')
    ground_point=[[-1.29,0,1.46],[1.85,0,1.46],[1.85,8.80,1.46],[-1.29,8.80,1.46],[-1.29,0,1.46]]
    x=[i[0] for i in ground_point]
    z=[i[1] for i in ground_point]
    y=[i[2] for i in ground_point]
    ax.plot(x,y,zs=z,linewidth=2,color='C0')
    ground_point=[[-1.29,3.34,1.46],[1.85,3.37,1.46]]
    x=[i[0] for i in ground_point]
    z=[i[1] for i in ground_point]
    y=[i[2] for i in ground_point]
    ax.plot(x,y,zs=z,linewidth=2,color='C0')

    #keypoint: O, A, B, C, D, E, F, O
    keypoint = [
        [0, 0, 0],
        [-1.29, 8.28, 1.46],
        [-1.29, 6.28, 1.46],
        [-1.29, 4.57, 1.46],
        [1.85, 8.07, 1.46],
        [1.85, 6.30, 1.46],
        [1.85, 4.62, 1.46],
    ]
    x=[i[0] for i in keypoint]
    z=[i[1] for i in keypoint]
    y=[i[2] for i in keypoint]
    keypoint_label = [
        "O",
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
    ]
    for i in range(len(x)):
        ax.scatter(x[i], y[i], zs=z[i],s=30)
        offset = 0.05
        ax.text(x[i]+offset, y[i]+offset, z[i]+offset,s=keypoint_label[i], fontsize=12)

    ax.legend()

    ax.set_ylim(-0.5, 1.5)
    ax.set_xlim(-1.5, 2)
    ax.set_zlim(-0.1, 10)

    #ax.set_zlim(-2, 10)
    #ax.set_ylim(-2, 10)
    #ax.set_xlim(-2, 10)

    ax.set_xlabel('X (m)')
    ax.set_ylabel('Y (m)')
    ax.set_zlabel('Z (m)')
    ax.set_box_aspect([1, 1, 2])
    ax.view_init(elev=-55, azim=-35, roll=-60)


#    plt.savefig('myplot.png')
    plt.show()


if __name__ == '__main__':
    #_main()
    draw_ground()

