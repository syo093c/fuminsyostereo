# FuminsyoStereo
![trakcing1](https://gitlab.com/syo093c/fuminsyostereo/-/raw/main/demo/s4.gif)
![tracking2](https://gitlab.com/syo093c/fuminsyostereo/-/raw/main/demo/s2.gif)

This is a library for 3D person detection, tracking, and behavior analysis based on stereo cameras. This library is licensed under MIT license.

You can use this library to:

1. Calibrate stereo camera (generate a ```pkl``` calibration file).
2. Perform depth estimation using stereo matching.
   
   Supports multiple algorithms.
   
   2.1 OpenCV block matching
   
   2.2 Google HitNet
   
   2.3 CREStereo (recommended)
3. Detect and estimate the 3D position of person.
4. Track person in 3D space and perform simple behavior analysis.
   
### Install
```
cd ../YOLOX
pip install -e .
cd ./tracking/cython_bbox_3d
pip install -e .
```

### Use
```
DepthEstimate.py              ->    Depth estimate 
Detection2d.py                ->    Detection in 2D
Detection3d.py                ->    Detection in 3D lefting from 2D
generate_calibrated_video.py  ->    generate demo
generate_disparity_video.py   ->    generate demo
Segmentation3d.py             ->    try for Segmentation
StereoCalibration.py          ->    Calibration
Track3d.py                    ->    Tracking in 3D space
```

### Todo
1. Implement detection and tracking from different viewpoints.
2. Use skeleton-based methods for behavior analysis.
3. Improve stereo matching performance using super-resolution methods.
