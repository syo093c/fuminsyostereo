# when use binary MegEngine and Pytorch, it will set $CUDNN$.
# Put MegEngine at first to make it functional.
 
from Detection3d import Detection3d
from CREStereo.test import load_model
from Detection3d import init_detection2d
from DepthEstimate import DepthEstimate
import cv2
from tqdm import tqdm
import os
import argparse

import sys
sys.path.append('./tracking/ByteTrack')
from tracking.ByteTrack.yolox.tracker.byte_tracker import BYTETracker
from utils.Visualiz import Visualiz
import numpy as np
from yolox.exp import get_exp
import pandas as pd


class Track3d():
    def __init__(self,detector:Detection3d) -> None:
        self.detector = detector
        args={}
        #args["track_thresh"]=0.5
        #args["track_buffer"]=30
        #args["mot20"]=False
        #args["match_thresh"]=0.8

        args["track_thresh"]=0.6
        args["track_buffer"]=40
        args["mot20"]=False
        args["match_thresh"]=0.8 # lager matching is easier

        self.tracker = BYTETracker(args=args)
    

def video_demo(t:Track3d, video_path):
    # delete cache
    cmd='rm -rfv ./video_tmp/*'
    os.system(cmd)
    cap=cv2.VideoCapture(video_path)
    width= cap.get(cv2.CAP_PROP_FRAME_WIDTH) # float
    height= cap.get(cv2.CAP_PROP_FRAME_HEIGHT) # float
    frame_number=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    
    v=Visualiz()
    v.init_canvas()
    export_results=[]
    for n in tqdm(range(frame_number)):
        ret, frame = cap.read()
        n+=1

        #print('Frame: '+str(n))

        if not ret:
        #if not ret or n==5:
            break
        
        # 2d image as input, get 3d information
        t.detector.update(frame)
        #t.detector.visualization_2d()
        bboxes_3d=t.detector.bboxes_3d
        scores=t.detector.scores
        output_tracks=t.tracker.update(output_results=np.array(bboxes_3d),scores=scores)

        points_3d=t.detector.dpe.xyz
        rep_3d=[]
        for i in output_tracks:
            print(i.mean)
            print(i.mean[6])
            print(i.mean[7])
            dx=i.mean[6]
            dz=i.mean[8]
            dv=(dx**2+dz**2)**0.5
            if dz != 0:
                angle=np.arctan([dz/dx])
                print(angle)
            else:
                angle=0
                print(0)
            #rep_3d.append(t.detector.generate_person_template(i.cexyz[:3],i.cexyz[3:6],rotation=angle))
            rep_3d.append(t.detector.generate_person_template(i.cexyz[:3],i.cexyz[3:6],rotation=0))
        #bboxes_3d=[t.detector.generate_person_template(i.cexyz[:3],i.cexyz[3:6],rotation=angle) for i in output_tracks]
        for i in output_tracks:
            export_results.append(np.append([n,i.track_id],i.mean))

        labels=[j.track_id for i,j in enumerate(output_tracks)]
        image = cv2.cvtColor(t.detector.dpe.img_left,cv2.COLOR_BGR2RGB)/255.
        point_colors=image.reshape(-1,3)

        # just visualize it with Open3D!!
        img=v.update(points=points_3d,
                point_colors=point_colors,
                ref_boxes=np.array(rep_3d),
                ref_labels=labels)
        
        cv2.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',img)
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+'3d_detection_result_'+os.path.basename(opt.input)
    os.system(cmd) 
    
    #export csv
    export_results=pd.DataFrame(export_results,columns=['frame_id','track_id','x','y','z','a','b','ey','vx','vy','vz','va','vb','vl'])
    export_results.to_csv('data.csv')        


def video(opt):
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    device='cuda:0'

    #use yolox weight
    exp=get_exp('./detection/YOLOX/exps/example/mot/yolox_x_mix_det.py')
    #det2d_model = init_detection2d(exp=exp,use_pose_model=False)
    det2d_model = init_detection2d(exp=exp,use_pose_model=True)
    
    #use bytetrack yolox 2d detector weight
    #exp=get_exp('./detection/YOLOX/exps/example/mot/yolox_x_mix_det_bytetrack.py')
    #det2d_model = init_detection2d(exp=exp,ckpt='detection/YOLOX/weight/bytetrack_x_mot17.pth.tar',use_pose_model=False)

    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)

    det3d=Detection3d(det2d_model,dpe)

    my_3d_tracker=Track3d(det3d)
    video_demo(my_3d_tracker,opt.input)


def debug(opt):
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    device='cuda:0'

    #use yolox weight
    exp=get_exp('./detection/YOLOX/exps/example/mot/yolox_x_mix_det.py')
    det2d_model = init_detection2d(exp=exp,use_pose_model=True)
    
    #use bytetrack yolox 2d detector weight
    #exp=get_exp('./detection/YOLOX/exps/example/mot/yolox_x_mix_det_bytetrack.py')
    #det2d_model = init_detection2d(exp=exp,ckpt='detection/YOLOX/weight/bytetrack_x_mot17.pth.tar',use_pose_model=False)

    dpe=DepthEstimate(model=crestereo_model,need_to_calibrate=1)

    det3d=Detection3d(det2d_model,dpe)

    mytracker=Track3d(det3d)
    
    cap=cv2.VideoCapture(opt.input)
    width= cap.get(cv2.CAP_PROP_FRAME_WIDTH) # float
    height= cap.get(cv2.CAP_PROP_FRAME_HEIGHT) # float
    frame_number=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    
    v=Visualiz()
    v.init_canvas()
    export_results=[]
    for n in tqdm(range(frame_number)):
        ret, frame = cap.read()
        n += 1
        if not ret:
            break
        img = frame
        result = det2d_model.detect_human(img)
        img = det2d_model.visual(img=img, result=result)
        cv2.imwrite('./video_tmp/'+str(n).zfill(6)+'.png', img)
    cmd = 'ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 ' + '2d_detection_result_'+os.path.basename(opt.input)
    os.system(cmd)

if __name__ == "__main__":
    arg=argparse.ArgumentParser()
    arg.add_argument('function',default='video')
    arg.add_argument('--input','-i',default='test1.mp4')
    opt=arg.parse_args()
    if opt.function == 'video':
        video(opt)
    if opt.function == 'debug':
        debug(opt)