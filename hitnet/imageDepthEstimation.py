from dis import dis
import cv2 as cv
import tensorflow as tf
import numpy as np

from hitnet import HitNet, ModelType, draw_disparity, draw_depth, CameraConfig, load_img

# Select model type
model_type = ModelType.middlebury
#model_type = ModelType.flyingthings
#model_type = ModelType.eth3d

if model_type == ModelType.middlebury:
    model_path = "models/middlebury_d400.pb"
elif model_type == ModelType.flyingthings:
    model_path = "models/flyingthings_finalpass_xl.pb"
elif model_type == ModelType.eth3d:
    model_path = "models/eth3d.pb"


# Initialize model
hitnet_depth = HitNet(model_path, model_type)

# Load images
#left_img = load_img("https://vision.middlebury.edu/stereo/data/scenes2003/newdata/cones/im2.png")
#right_img = load_img("https://vision.middlebury.edu/stereo/data/scenes2003/newdata/cones/im6.png")

img=cv.imread('/home/syo/d.png')
img=cv.resize(img,(img.shape[1]//2,img.shape[0]//2),interpolation= cv.INTER_LINEAR)
imgL=img[:img.shape[0],:int(img.shape[1]/2)]
imgR=img[:img.shape[0],int(img.shape[1]/2):]
# Estimate the depth
disparity_map = hitnet_depth(imgL, imgR)
depth_map = hitnet_depth.get_depth()


color_disparity = draw_disparity(disparity_map)
cobined_image = np.hstack((imgL, imgR, color_disparity))

cv.namedWindow("Estimated disparity", cv.WINDOW_NORMAL)	
#cv.imshow("Estimated disparity", cobined_image)
#print(img)
cv.imshow("Estimated disparity", color_disparity)
cv.waitKey(0)

cv.imwrite("out.jpg", cobined_image)
cv.imwrite("disparity.jpg", color_disparity)

cv.destroyAllWindows()