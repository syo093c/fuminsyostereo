from DepthEstimate import DepthEstimate

from mmseg.apis import inference_segmentor, init_segmentor, show_result_pyplot
from mmseg.core.evaluation import get_palette

from DepthEstimate import DepthEstimate
import cv2 as cv
from CREStereo.test import load_model
import numpy as np
import open3d as o3d
import sys
import argparse
#np.set_printoptions(threshold=sys.maxsize)

class Segmentation2d():
    def __init__(self,model:init_segmentor) -> None:
        self.model=model

    def update(self,img): 
        self.segmentation_map=inference_segmentor(self.model,img)

class Segmentation3d():
    def __init__(self,mmseg2d:Segmentation2d,dpe:DepthEstimate) -> None:
        self.dpe=dpe
        self.mmseg=mmseg2d

    def update(self,img):
        self.dpe.update(img)
        self.img_left=self.dpe.img_left
        self.mmseg.update(self.img_left)
        self.disparity_map=np.array(self.dpe.disparity_map)
        self.segmentation_map=np.array(self.mmseg.segmentation_map[0])

    def visualization(self):
        a=self.segmentation_map
        dmap=self.disparity_map
        dmap[a!=0]=0
        b=np.zeros((1080, 1920, 3))
        b[:,:,0]=a
        b[:,:,1]=a//2
        b[:,:,2]=a//2
        b=b.astype(np.uint8)
        print(self.segmentation_map.shape)
        #rgbd_image=o3d.geometry.RGBDImage.create_from_color_and_depth(o3d.geometry.Image(cv.cvtColor(self.img_left, cv.COLOR_BGR2RGB)),
        #    o3d.geometry.Image(1054*0.12/self.disparity_map),convert_rgb_to_intensity=False)

        rgbd_image=o3d.geometry.RGBDImage.create_from_color_and_depth(o3d.geometry.Image(b),
                    o3d.geometry.Image(1054*0.12/self.disparity_map),convert_rgb_to_intensity=False)
        pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
        rgbd_image,
        o3d.camera.PinholeCameraIntrinsic(1920,1080,1054,1054,1920/2,1080/2))

        coord=o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.0005)
        o3d.visualization.draw_geometries([pcd,coord])

def test_canny():
    img=cv.imread('mpv-shot0002.jpg')
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model)
    dpe.update(img)
    img_left=dpe.img_left
    img_left_blur=cv.GaussianBlur(img_left, (5, 5), 0)
    #edges = cv.Canny(img_left, threshold1=100, threshold2=200)
    edges = cv.Canny(img_left, threshold1=50, threshold2=100)
    cv.imshow("Edges", edges)
    cv.waitKey(0)
    cv.destroyAllWindows()

def test_dump():
    img=cv.imread('mpv-shot0002.jpg')
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    dpe=DepthEstimate(model=crestereo_model)
    dpe.update(img)
    dpe.convert2pcd()
    dpe.visualization_rgb()

    
def main():
    img=cv.imread('mpv-shot0002.jpg')
    crestereo_model=load_model('./CREStereo/crestereo_eth3d.mge')
    config='mmsegmentation/configs/pspnet/pspnet_r101-d8_512x512_4x4_80k_coco-stuff164k.py'
    checkpoint='mmsegmentation/weight/pspnet_r101-d8_512x512_4x4_80k_coco-stuff164k_20210707_152034-7eb41789.pth'
    device='cuda:0'
    seg_model = init_segmentor(config, checkpoint, device=device)

    seg2d=Segmentation2d(seg_model)
    dpe=DepthEstimate(model=crestereo_model)

    seg3d=Segmentation3d(seg2d,dpe)
    seg3d.update(img)
    seg3d.visualization()
    #seg3d.dpe.visualization_rgb()

if __name__ == '__main__':
    arg=argparse.ArgumentParser()
    arg.add_argument('function',default='main')
    opt=arg.parse_args()
    if opt.function == 'main':
        main()
    elif opt.function == 'test_canny':
        test_canny()
    elif opt.function == 'test_dump':
        test_dump()