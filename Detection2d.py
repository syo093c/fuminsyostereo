import argparse
import os
import time
from loguru import logger
import warnings
import numpy as np
from tqdm import tqdm

import cv2

import torch
import sys
from pathlib import Path

sys.path.append(str(Path.joinpath(Path.cwd(),'detection/YOLOX')))
from yolox.data.data_augment import ValTransform
from yolox.data.data_augment import preproc_mean_std

from yolox.data.datasets import COCO_CLASSES
from yolox.exp import get_exp
from yolox.utils import fuse_model, get_model_info, postprocess, vis

from mmpose.apis import (inference_top_down_pose_model, init_pose_model,
                         vis_pose_result)
from mmpose.datasets import DatasetInfo

def _init_pose_model():
    pose_config='./mmpose/configs/body/2d_kpt_sview_rgb_img/topdown_heatmap/coco/hrnet_w48_coco_256x192.py'
    pose_checkpoint='https://download.openmmlab.com/mmpose/top_down/hrnet/hrnet_w48_coco_256x192-b9e0b3ab_20200708.pth'
    device='cuda:0'
    model=init_pose_model(pose_config,pose_checkpoint,device)
    return model

class Predictor(object):
    def __init__(
        self,
        model,
        exp,
        cls_names=COCO_CLASSES,
        decoder=None,
        device="gpu",
        fp16=False,
        legacy=False,
        use_pose_model=False,
        #pose_model=None,
    ):
        """
        Init YOLOX detector,
        if @use_pose_model==True, then load mmpose HRNet as pose detector.
        """
        self.model = model
        self.cls_names = cls_names
        self.decoder = decoder
        self.num_classes = exp.num_classes
        self.confthre = exp.test_conf
        self.nmsthre = exp.nmsthre
        self.test_size = exp.test_size
        self.device = device
        self.fp16 = fp16
        self.preproc = ValTransform(legacy=legacy)
        self.byte_track_weight=False
        
        #--human pose----------
        self.use_pose_model=use_pose_model
        if self.use_pose_model == True:
            self.pose_model=_init_pose_model()

            self.pose_dataset = self.pose_model.cfg.data['test']['type']
            self.pose_dataset_info = self.pose_model.cfg.data['test'].get(
                'dataset_info', None)
            if self.pose_dataset_info is None:
                warnings.warn(
                    'Please set `dataset_info` in the config.'
                    'Check https://github.com/open-mmlab/mmpose/pull/663 for details.',
                    DeprecationWarning)
            else:
                self.pose_dataset_info = DatasetInfo(self.pose_dataset_info)

        

    def inference(self, img):
        """
        The raw function of YOLOX,
        @postprocess will nms(Non-maximum Suppression) bboxes with
        @self.threadhold.
        
        Args:
            img: The left raw image, calibrated.
                 The image will be reshaped before put into network,
                 the output coordinate is in processed image.
        
        Returns:
            outputs: bboxes in processed image, include all classes.
            img_info: raw image, raw image information for processing
                      outputs.
        """
        
        img_info = {"id": 0}
        if isinstance(img, str):
            img_info["file_name"] = os.path.basename(img)
            img = cv2.imread(img)
        else:
            img_info["file_name"] = None

        height, width = img.shape[:2]
        img_info["height"] = height
        img_info["width"] = width
        img_info["raw_img"] = img

        if self.byte_track_weight:
            # bytetrack yolox weight
            self.rgb_means = (0.485, 0.456, 0.406)
            self.std = (0.229, 0.224, 0.225)
            img, ratio = preproc_mean_std(img, self.test_size, self.rgb_means, self.std)
        else:
            #default function 
            ratio = min(self.test_size[0] / img.shape[0], self.test_size[1] / img.shape[1])
            img, _ = self.preproc(img, None, self.test_size)

        img_info["ratio"] = ratio
        img = torch.from_numpy(img).unsqueeze(0)
        img = img.float()
        if self.device == "gpu":
            img = img.cuda()
            if self.fp16:
                img = img.half()  # to FP16

        with torch.no_grad():
            t0 = time.time()
            outputs = self.model(img)
            if self.decoder is not None:
                outputs = self.decoder(outputs, dtype=outputs.type())
            outputs = postprocess(
                outputs, self.num_classes, self.confthre,
                self.nmsthre, class_agnostic=True
            )
            #logger.info("Infer time: {:.4f}s".format(time.time() - t0))
        return outputs, img_info

    def detect(self,img):
        """
        A wrapper function of @self.inference.
        To return resized bboxes, contains all classes.
        """

        outputs, img_info = self.inference(img)
        output=outputs[0].cpu()
        #person_mask=output[:,6]==0
        #output=output[person_mask]

        ratio = img_info["ratio"]
        bboxes = output[:, 0:4]
        # preprocessing: resize
        bboxes /= ratio
        classes=output[:,6]

        return bboxes, classes

    def detect_human_with_pose(self,img):
        """
        A wrapper function of @self.inference.
        To return resized bboxes, and only return human class.
        If needed, also return every human pose with confidence.
        
        Args: The left calibtrated image.

        Returns: 
            @use_pose_model==False, bboxes
            @use_pose_model==True, 
            pose_result(dictionary list, [{'bbox': array,'keypoints': array}])

        """

        #Inference results: [left,top,right,bottom,obj_conf,cls_conf,cls_id]
        outputs, img_info = self.inference(img)
        #print(outputs)
        output=outputs[0].cpu()
        person_mask=output[:,6]==0
        output=output[person_mask]
        
        #occlusion_mask
        o_mask=output[:,5]>0.8
        output=output[o_mask]

        ratio = img_info["ratio"]
        #bboxes = output[:, 0:5]
        bboxes = output
        # preprocessing: resize, do not resize the score
        bboxes[:,:4] /= ratio
        classes=output[:,6]
        #print(bboxes)

        #resturn pose detection results
        self.pose_dataset = self.pose_model.cfg.data['test']['type']
        self.pose_dataset_info = self.pose_model.cfg.data['test'].get(
            'dataset_info', None)
        if self.pose_dataset_info is None:
            warnings.warn(
                'Please set `dataset_info` in the config.'
                'Check https://github.com/open-mmlab/mmpose/pull/663 for details.',
                DeprecationWarning)
        else:
            self.pose_dataset_info = DatasetInfo(self.pose_dataset_info)


        person_results=[{'bbox':i.numpy().tolist()} for i in bboxes]
        #print(person_results)
        pose_results, returned_outputs=inference_top_down_pose_model(
        self.pose_model,
        img,
        person_results,
        bbox_thr=None,
        format='xyxy',
        dataset=self.pose_dataset,
        dataset_info=self.pose_dataset_info,
        return_heatmap=False,
        outputs=None)

        #bboxes=[i['bbox'] for i in pose_results]
        #keypoints=[i['keypoints'] for i in pose_results]
        #return bboxes, keypoints
        return pose_results
        
    def detect_human(self,img):
        """
        A wrapper function of @self.inference.
        To return resized bboxes, and only return human class.
        If needed, also return every human pose with confidence.
        
        Args: The left calibtrated image.

        Returns: 
            @use_pose_model==False, bboxes
            @use_pose_model==True, 
            pose_result(dictionary list, [{'bbox': array,'keypoints': array}])

        """

        #Inference results: [left,top,right,bottom,obj_conf,cls_conf,cls_id]
        outputs, img_info = self.inference(img)
        #print(outputs)
        output=outputs[0].cpu()
        person_mask=output[:,6]==0
        output=output[person_mask]
        
        #occlusion_mask
        o_mask=output[:,5]>0.8
        output=output[o_mask]

        ratio = img_info["ratio"]
        #bboxes = output[:, 0:5]
        bboxes = output
        # preprocessing: resize, do not resize the score
        bboxes[:,:4] /= ratio
        classes=output[:,6]
        #print(bboxes)

        #return bboxes, classes
        return bboxes

    def _visual(self, output, img_info, cls_conf=0.35):
        """
        The raw visual function from YOLOX,
        visualise output using YOLOX @vis api.
        """
        ratio = img_info["ratio"]
        img = img_info["raw_img"]
        if output is None:
            return img
        output = output.cpu()

        bboxes = output[:, 0:4]

        # preprocessing: resize
        bboxes /= ratio

        cls = output[:, 6]
        scores = output[:, 4] * output[:, 5]

        vis_res = vis(img, bboxes, scores, cls, cls_conf, self.cls_names)
        return vis_res

    def visual(self,result,img):
        """
        Visualise result, if @use_pose_model==False, just visulise bboxes.
        Else visulise bboxes and skeleton.

        Args:
            result: Bboxes or bboxes and keypoints.
            img: The raw left calibrated image.

        Returns: None
        """
        color=(255,0,0)
        if self.use_pose_model == False:
            for i,box in enumerate(result):
                x0 = int(box[0])
                y0 = int(box[1])
                x1 = int(box[2])
                y1 = int(box[3])

                cv2.rectangle(img, (x0, y0), (x1, y1), color, 2)
                cv2.circle(img,(int(0.5*(x0+x1)),int(0.5*(y0+y1))),2,(0,255,0),-1)
            return img
        else:
            bboxes=[i['bbox'] for i in result]
            skeleton_keypoints=[i['keypoints'] for i in result]
            for i,box in enumerate(bboxes):
                x0 = int(box[0])
                y0 = int(box[1])
                x1 = int(box[2])
                y1 = int(box[3])

                color=(255,0,0)
                cv2.rectangle(img, (x0, y0), (x1, y1), color, 2)

            # COCO dataset
            line_sk = [[15, 13], [13, 11], [16, 14], [14, 12], [11, 12],
                [5, 11], [6, 12], [5, 6], [5, 7], [6, 8], [7, 9],
                [8, 10], [1, 2], [0, 1], [0, 2], [1, 3], [2, 4],
                [3, 5], [4, 6]]
            for skeleton in skeleton_keypoints:
                for point in skeleton:
                    cv2.circle(img,(int(point[0]),int(point[1])),5,color,-1)
                for i in line_sk:
                    start_point=int(skeleton[i[0]][0]),int(skeleton[i[0]][1])
                    end_point=int(skeleton[i[1]][0]),int(skeleton[i[1]][1])
                    cv2.line(img,start_point,end_point,color,2)
            return img

def image_demo(predictor,img):
    outputs, img_info = predictor.inference(img)
    output=outputs[0].cpu()
    person_mask=output[:,6]==0
    output=output[person_mask]
    result_image = predictor.visual(output, img_info, predictor.confthre)
    cv2.imshow(mat=result_image,winname='test')
    cv2.waitKey(0)

def main(exp):
    model = exp.get_model()
    logger.info("Model Summary: {}".format(get_model_info(model, exp.test_size)))
    model.cuda()
    model.eval()
    logger.info("loading checkpoint")
    ckpt = torch.load('detection/YOLOX/weight/yolox_x.pth', map_location="cpu")
    # load the model state dict
    model.load_state_dict(ckpt["model"])
    logger.info("loaded checkpoint done.")

    predictor = Predictor(
        model, exp, COCO_CLASSES
    )
    img=cv2.imread('./detection/YOLOX/datasets/mpv-shot0002.jpg')
    image_demo(predictor=predictor,img=img)

    
def pose_test(exp):
    arg=argparse.ArgumentParser()
    arg.add_argument('--input','-i',default='test.mp4')
    opt=arg.parse_args()

    #remove exist directory
    if os.path.exists('video_tmp'):
        os.system("rm -rf ./video_tmp")
        os.mkdir("video_tmp")

    # init model
    model = exp.get_model()
    model.cuda()
    model.eval()
    ckpt = torch.load('detection/YOLOX/weight/yolox_x.pth', map_location="cpu")
    model.load_state_dict(ckpt["model"])
    predictor = Predictor(
        model, exp, COCO_CLASSES, use_pose_model=True
    )

    cap=cv2.VideoCapture(opt.input)
    n=0
    frame_number=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    print(frame_number)

    for n in tqdm(range(frame_number)):
        ret,frame=cap.read()
        n+=1
        if not ret:
            break
        img=frame
        result=predictor.detect_human(img)
        img=predictor.visual(img=img,result=result)
        cv2.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',img)
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+'2d_detection_result_'+os.path.basename(opt.input)
    os.system(cmd) 

def init_detection2d(exp=None,ckpt=None,use_pose_model=False):
    """
    init 2d detector use yolox
    @exp.test_conf is the thred value for confidence
    """
    if exp is None:
        exp = get_exp(exp_name='yolox-x')
        #exp.test_conf=0.7
    model = exp.get_model()
    logger.info("Model Summary: {}".format(get_model_info(model, exp.test_size)))
    model.cuda()
    model.eval()
    logger.info("loading checkpoint")
    if ckpt is None:
        ckpt = torch.load('detection/YOLOX/weight/yolox_x.pth', map_location="cpu")
    else:
        ckpt=torch.load(ckpt,map_location='cpu')
    # load the model state dict
    model.load_state_dict(ckpt["model"])
    logger.info("loaded checkpoint done.")

    predictor = Predictor(
        model, exp, COCO_CLASSES,use_pose_model=use_pose_model
    )
    print(exp)
    return predictor

def test(exp):
    arg=argparse.ArgumentParser()
    arg.add_argument('--input','-i',default='test.mp4')
    opt=arg.parse_args()

    #remove exist directory
    if os.path.exists('video_tmp'):
        os.system("rm -rf ./video_tmp")
        os.mkdir("video_tmp")

    # init model
    model = exp.get_model()
    model.cuda()
    model.eval()
    #ckpt = torch.load('detection/YOLOX/weight/yolox_x.pth', map_location="cpu")
    #model.load_state_dict(ckpt["model"])
    #predictor = Predictor(
    #    model, exp, COCO_CLASSES, use_pose_model=False
    #)

    exp=get_exp('./detection/YOLOX/exps/example/mot/yolox_x_mix_det.py')
    predictor = init_detection2d(exp=exp,use_pose_model=False)


    cap=cv2.VideoCapture(opt.input)
    n=0
    frame_number=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    print(frame_number)

    for n in tqdm(range(frame_number)):
        ret,frame=cap.read()
        n+=1
        if not ret:
            break
        img=frame
        result=predictor.detect_human(img)
        img=predictor.visual(img=img,result=result)
        cv2.imwrite('./video_tmp/'+str(n).zfill(6)+'.png',img)
    cmd='ffmpeg -f image2 -i ./video_tmp/%06d.png -c:v h264_nvenc -qp 0 '+'2d_detection_result_'+os.path.basename(opt.input)
    os.system(cmd) 
    
if __name__ == "__main__":
    exp = get_exp(exp_name='yolox-x')
    exp.test_conf=0.7
    #print(exp)
    #main(exp)
    #pose_test(exp)
    test(exp)